#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "errors.h"

static const char* const read_status_messages[] = {
    [READ_OK] = "success.",
    [READ_INVALID_BITS] = "read an invalid bits.",
    [READ_INVALID_HEADER] = "read an invalid header.",
    [READ_ERROR] = "failed to read."
};

static const char* const write_status_messages[] = {
    [WRITE_OK] = "success.",
    [WRITE_ERROR] = "failed to write.",
    [UNABLE_WRITE_IMAGE] = "unable to write image.",
    [WRITE_INVALID_BITS] = "write an invalid bits."
};

const char* get_read_status_message(enum read_status r_st) {
    return read_status_messages[r_st];
}

const char* get_write_status_message(enum write_status w_st) {
    return write_status_messages[w_st];
}

void err(const char* msg, ...) {
	va_list args;
	va_start(args, msg);
	vfprintf(stderr, msg, args);
	va_end(args);
	exit(1);
}
